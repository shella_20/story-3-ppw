from django.shortcuts import render

def about(request):
    return render(request, 'about.html')

def experience(request):
    return render(request, 'experience.html')

def homepage(request):
    return render(request, 'homepage.html')

def skills(request):
    return render(request, 'skills.html')
# Create your views here.

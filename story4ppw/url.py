from django.urls import path
from . import views

app_name = 'story4ppw'

urlpatterns = [
    path('about/', views.index, name='about'),
    path('experience/', views.profile, name='experience'),
    path('homepage/', views.index, name='homepage'),
    path('skills/', views.profile, name='skills'),
    # dilanjutkan ...
]